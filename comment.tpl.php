<div class="commentbody <?php print ($comment->new) ? 'comment-new' : '' ?>"> 
  <?php if ($comment->new) : ?> 
  <a id="new"></a> <span class="new"><?php print $new ?></span> 
  <?php endif; ?> 
  <h4 class="c_userlink"><?php print $author ?> wrote:</h4> 
  <div class="c_commentbody"><?php print $content ?></div> 
  <?php if ($picture) : ?> 
  <br class="clear" /> 
  <?php endif; ?> 
  <div class="c_commentinfo"><?php print $date ?></div>
  <p class="postmetadata"><?php print $links ?> &#187;</p> 
</div>