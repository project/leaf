<div class="contentbody<?php print ($sticky) ? " sticky" : ""; ?>"> 
  <?php if ($page == 0): ?> 
  <h3><a href="<?php print $node_url ?>" rel="bookmark" title="Read Entry: <?php print $title ?>"><?php print $title ?></a></h3> 
  <?php endif; ?> 
  <?php print $content ?>
  <?php if ($links): ?> 
  <div class="itemdetails">
  <span class="item2"><?php print $terms ?></span>
  <span class="item3"><?php print $links ?></span>
  </div> 
  <?php endif; ?> 
</div>