<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
<title><?php print $head_title ?></title>
<meta http-equiv="Content-Style-Type" content="text/css" />
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
</head>

<body id="home" <?php print theme("onload_attribute"); ?>>
	<div id="header">
		<h1><a href="<?php print url() ?>" title="<?php print($site_name) ?>"><?php print($site_name) ?></a></h1>
		<div class="slogan"><?php print($site_slogan) ?></div>
	        <?php if (isset($primary_links)) : ?>
		  <div id="navigation"><h3 class="hidden">Navigation</h3>
		  <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
    		</div>
		<?php endif; ?>
	</div>
<div id="wrapper">
	<div id="container">
		<div id="content">
			<div class="contentdiv">
				<h2><?php print($site_name) ?></h2>
				<div class="divweblog">
					<div class="navigation"> <?php print $breadcrumb ?> </div>
					<?php if ($messages != ""): ?>
					<div id="message"><?php print $messages ?></div>
					<?php endif; ?>
					<?php if ($mission != ""): ?>
					<div id="mission"><?php print $mission ?></div>
					<?php endif; ?>
					<?php if ($title != ""): ?>
					<h2 class="page-title"><?php print $title ?></h2>
					<?php endif; ?>
					<?php if ($tabs != ""): ?>
					<?php print $tabs ?>
					<?php endif; ?>
					<?php if ($help != ""): ?>
					<p id="help"><?php print $help ?></p>
					<?php endif; ?>
					<!-- start main content -->
					<?php print($content) ?>
					<!-- end main content -->
	      			</div>
    			</div>
  		</div>
	</div>
<div id="sidebar">
	<?php if ($search_box): ?>
		<?php print $search_box ?>
	<?php endif; ?>
	<?php print $left ?>
	<?php print $right ?>
</div>

<div class="clearing">&nbsp;</div>

<!--Close Wrapper-->
</div>

<div id="footer">
	<div id="credits">Design by <a href="http://www.arvoriad.com">Arvoriad.com</a> | <a href="http://validator.w3.org/check?uri=referer">XHTML 1.0 Strict</a> | <a href="http://jigsaw.w3.org/css-validator/">CSS</a><br /></div>
</div>
<?php print $closure;?>
</body>
</html>